package permissions.db;

import java.util.List;

import permissions.domain.Role;

public interface RoleRepository extends Repository<Role> {

	public List<Role> withrole(String role, PagingInfo page);
	public List<Role> withpermmision(String permmisions, PagingInfo page);
	
}
