package permissions.db.catalogs;

import java.sql.Connection;

import permissions.db.AddressRepository;
import permissions.db.PersonRepository;
import permissions.db.RoleRepository;
import permissions.db.repos.HsqlAddressRepository;
import permissions.db.repos.HsqlPermissionsRepository;
import permissions.db.repos.HsqlPersonRepository;
import permissions.db.repos.HsqlRoleRepository;
import permissions.db.repos.HsqlUserRepository;

public class HsqlRepositoryCatalog {

Connection connection;
	
	public HsqlRepositoryCatalog(Connection connection) {
		this.connection = connection;
	}

	public PersonRepository people() {
		return new HsqlPersonRepository(connection);
	}

	public AddressRepository address()
	{
		return new HsqlAddressRepository(connection);
	}
	public PersonRepository person()
	{
	
		return new HsqlPersonRepository(connection);
	}
	
	public RoleRepository role()
	
	{
		
		return new HsqlRoleRepository(connection);
		
	}
	
	public HsqlPermissionsRepository permission(){
		
		return new HsqlPermissionsRepository(connection);
	}
	
	public HsqlUserRepository user(){
		return new HsqlUserRepository(connection);
	}
}
