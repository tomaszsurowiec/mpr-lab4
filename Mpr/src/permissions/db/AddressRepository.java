package permissions.db;


	import java.util.List;

import permissions.domain.Address;


	public interface AddressRepository extends Repository<Address> {
		
		public List<Address> withCity(String City, PagingInfo page);
		public List<Address> withCountry(String Country, PagingInfo page);
		
	}
	

