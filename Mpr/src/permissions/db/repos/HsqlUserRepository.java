package permissions.db.repos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import permissions.db.PagingInfo;
import permissions.db.UserRepository;
import permissions.domain.User;


public class HsqlUserRepository implements UserRepository {

private Connection connection;
	
	private String insertSql ="INSERT INTO user(name,surname) VALUES(?,?)"; 
	private String selectSql ="SELECT * FROM user LIMIT(?,?)";
	private String selectByLoginSql ="SELECT * FROM user WHERE id=?";
	private String selectByPasswordSql ="SELECT * FROM user WHERE password=? LIMIT(?,?)";
	private String deleteSql = "DELETE FROM user WHERE login=?";
	private String updateSql = "UPDATE user SET (password,login)=(?,?) WHERE id=?";
	
	private PreparedStatement insert;
	private PreparedStatement select;
	private PreparedStatement selectById;
	private PreparedStatement selectByPassword;
	private PreparedStatement selectByLogin;
	private PreparedStatement delete;
	private PreparedStatement update;
	

	private String createUserTable =""
			+ "CREATE TABLE User("
			+ "id bigint GENERATED BY DEFAULT AS IDENTITY,"
			+ "login VARCHAR(20),"
			+ "password VARCHAR(50)"
			+ ")";
	
	
	public HsqlUserRepository(Connection connection){
		this.connection=connection;
		
		try{
			

			insert = connection.prepareStatement(insertSql);
			select = connection.prepareStatement(selectSql);
			selectByLogin = connection.prepareStatement(selectByLoginSql);
			selectByPassword = connection.prepareStatement(selectByPasswordSql);
			delete = connection.prepareStatement(deleteSql);
			update = connection.prepareStatement(updateSql);
			
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean tableExists =false;
			while(rs.next())
			{
				if(rs.getString("TABLE_NAME").equalsIgnoreCase("User")){
					tableExists=true;
					break;
				}
			}
			if(!tableExists){
				Statement createTable = connection.createStatement();
				createTable.executeUpdate(createUserTable);
			}
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		
	}

	public List<User> allOnPage(PagingInfo page) {
		List<User> result = new ArrayList<User>();
		
		try {
			select.setInt(1, page.getCurrentPage()*page.getSize());
			select.setInt(2, page.getSize());
			ResultSet rs = select.executeQuery();
			while(rs.next()){
				User user = new User();
				user.setPassword(rs.getString("password"));
				user.setUsername(rs.getString("Username"));
				user.setId(rs.getShort("Id"));
				result.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}


	public void add(User u) {
		try {
			insert.setString(1,u.getPassword() );
			insert.setString(2,u.getUsername() );
			
			insert.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
		
	

	public void modify(User u) {
		
		try {
			update.setString(1, u.getPassword());
			update.setString(2, u.getUsername());
			update.setInt(3, u.getId());
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	public void remove(User u) {
		try {
			delete.setInt(1, u.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	public List<User> withusername(String username, PagingInfo page) {
		List<User> result = new ArrayList<User>();
		try {
			selectByLogin.setString(1, username );
			selectByLogin.setInt(2, page.getCurrentPage()*page.getSize());
			ResultSet rs = selectByLogin.executeQuery();
			while(rs.next()){
				User user = new User();
				user.setUsername(rs.getString("username"));
				user.setId(rs.getInt(rs.getInt("id")));
				result.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	


	
	public List<User> withpassword(String password, PagingInfo page) {
		List<User> result = new ArrayList<User>();
		try {
			selectByPassword.setString(1, password );
			selectByPassword.setInt(2, page.getCurrentPage()*page.getSize());
			ResultSet rs = selectByPassword.executeQuery();
			while(rs.next()){
				User user = new User();
				user.setPassword(rs.getString("password"));
				user.setId(rs.getShort("id"));
				result.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
		

	public User withId(int id) {
		
		User result = null;
		try {
			selectById.setInt(1, id);
			ResultSet rs = selectById.executeQuery();
			while(rs.next()){
				User user = new User();
				user.setUsername(rs.getString("user"));
				user.setId(rs.getInt("id"));
				result = user;
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
		
	
	
}
	
	
	