package permissions.db;

import java.util.List;

import permissions.domain.Permission;

public interface PermissionsRepository extends Repository<Permission> {

	
	public List<Permission> withName(String Name, PagingInfo page);
	
	
}
