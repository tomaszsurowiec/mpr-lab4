package permissions.db;

import java.util.List;

import permissions.domain.User;


public interface UserRepository extends Repository<User> {

	public List<User>withusername(String username, PagingInfo page);
	public List<User>withpassword(String password, PagingInfo page);
	
}
